import React, { Component } from 'react';

class Twitter extends React.Component {
  render() {
    return (
        //<body class="bg-grey-light">
        
        <div className="Twitter">a
            <div class = "bg-white">
                <div class="container mx-auto lg:flex md:block items-center py-4">
                    <div class="lg:hidden md:w-full text-center"> <a href="#"><i class="fab fa-twitter fa-lg text-blue"></i></a></div>
                    <div class="lg:w-2/5 md:w-full md:text-center">
                        <a href="#" class = "text-grey-darker mr-4 pb-6  hover:text-teal"><i class="fa fa-home"> </i>Home</a>
                        <a href="#" class = "text-grey-darker mr-4 pb-6  hover:text-teal"><i class="fa fa-bolt"> </i>Moment</a>
                        <a href="#" class = "text-grey-darker mr-4 pb-6  hover:text-teal"><i class="fa fa-bell"> </i>Notification</a>
                        <a href="#" class = "text-grey-darker mr-4 pb-6  hover:text-teal"><i class="fa fa-envelope"> </i>Message</a>
                    </div>
                    <div class="md:hidden lg:block lg:w-1/5 text-center"> <a href="#"><i class="fab fa-twitter fa-lg text-blue"></i></a></div>
                    <div class="lg:w-2/5 md:w-full flex lg:justify-end md:py-3 md:justify-center">
                        <div class="relative"> 
                            <input type="text" class="bg-grey-lighter h-8 px-4 py-2 text-xs w-48 rounded-full" placeholder="Search" />
                            <span class="flex item-center absolute pin-r pin-y mr-3 my-2"><i class="fas fa-search"></i></span>
                        </div>
                        <div> 
                        </div>
                        <div> 
                            <button class="bg-teal hover:bg-teal-dark text-white py-2 px-4 rounded-full">Tweet</button>
                        </div>
                    </div>
                </div> 



                <div class="h-64 bg-blue-light"> </div>


                <div class="bg-white shadow">
                    <div class="container mx-auto flex items-center relative">
                        <div class="w-1/4">
                            <img src="https://www.qualiscare.com/wp-content/uploads/2017/08/default-user.png" alt="logo" class="rounded-full h-48 w-48 absolute pin-l pin-t -mt-20" />
                        </div>

                        <div class="w-1/2">
                            <ul class="list-reset flex">
                                <li class="text-center py-3 px-4 border-b-2 border-solid border-teal"> 
                                    <a href="#" class="text-grey-darker">
                                        <div>Tweets</div>
                                        <div>68</div>
                                    </a>
                                </li>

                                <li class="text-center py-3 px-4 border-b-2 border-solid "> 
                                    <a href="#" class="text-grey-darker">
                                        <div>Tweets</div>
                                        <div>68</div>
                                    </a>
                                </li>

                                <li class="text-center py-3 px-4 border-b-2 border-solid "> 
                                    <a href="#" class="text-grey-darker">
                                        <div>Tweets</div>
                                        <div>68</div>
                                    </a>
                                </li>

                                <li class="text-center py-3 px-4 border-b-2 border-solid "> 
                                    <a href="#" class="text-grey-darker">
                                        <div>Tweets</div>
                                        <div>68</div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="w-1/4">
                            <div>
                                <button class="bg-teal hover:bg-teal-dark text-white py-2 px-4 rounded-full">Following</button>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>

            <div class="container lg:flex md:block mx-auto mt-3">
                <div class="lg:w-1/4  md:w-full pr-6 mt-8 mb-4 md:py-4">
                    <h1>Tailwind CSS</h1>
                </div>
                <div class="lg:w-1/2  md:w-full bg-white mb-4">
                    <div class="p-3 text-lg border-b border-solid border-grey-light">
                        <a href="#" class="text-black mr-6" > Tweets </a>
                        <a href="#" class="text-teal mr-6" > Reply </a>
                        <a href="#" class="text-teal mr-6" > Media </a>
                    </div>

                    <div class="flex border-b border-solid border-grey"> 
                        <div class="w-1/5 text-right pl-3 pt-3">
                            <div><i class="fas fa-thumbtack text-teal"></i></div>
                            <div>
                                <a href="#"><img src="https://www.qualiscare.com/wp-content/uploads/2017/08/default-user.png" alt="avatar" class="rounded-full h-12 w-12 mr-2" />
                                </a>
                                </div>
                        </div>
                        <div class="w-4/5 p-3 pl-0">
                            <div class="text-xs text-grey-dark">Pinned Tweet</div>
                            <div class="flex justify-between">
                                <div>
                                    <span class="font-bold"><a href="#" class="text-black">Tailwind CSS</a></span>
                                    <span class="text-grey-dark">@tailwindcss</span>
                                    <span class="text-grey-dark">&middot;</span>
                                    <span class="text-grey-dark">15 Dec 2017</span>
                                </div>
                                <div>
                                    <a href="#" class="text-grey-dark hover:text-teal"><i class="fa fa-chevron-down"></i></a>
                                </div>
                            </div>

                            <div>
                                <div class="mb-4">
                                    <p class="mb-6">🎉 Tailwind CSS v0.4.0 is out!</p>
                                    <p class="mb-6">Makes `apply` more useful when using !important utilities, and includes an improved default color palette:</p>
                                    <p class="mb-4"><a href="#">github.com/tailwindcss/ta...</a></p>
                                    <p> 
                                        <a href="#">
                                            <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/tt_tweet1.jpg" alt="tweet image" class="border border-solid border-grey-light rounded-sm" />
                                        </a>
                                    </p>
                                </div>
                            </div>

                            <div class="pb-2">
                                <span class="mr-8"><a href="#" class="text-grey-dark hover:no-underline hover:text-blue-light"><i class="fa fa-comment fa-lg mr-2"></i> 9</a></span>
                                <span class="mr-8"><a href="#" class="text-grey-dark hover:no-underline hover:text-green"><i class="fa fa-retweet fa-lg mr-2"></i> 29</a></span>
                                <span class="mr-8"><a href="#" class="text-grey-dark hover:no-underline hover:text-red"><i class="fa fa-heart fa-lg mr-2"></i> 135</a></span>
                                <span class="mr-8"><a href="#" class="text-grey-dark hover:no-underline hover:text-teal"><i class="fa fa-envelope fa-lg mr-2"></i></a></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="lg:w-1/4  md:w-full lg:pl-4 md:pl-0">
                            <div class="bg-white p-3 mb-3 md:mb-0">
                                <div>
                                    <span class="text-lg font-bold">Who to follow</span>
                                    <span>&middot;</span>
                                    <span><a href="#" class="text-xs">Refresh</a></span>
                                    <span>&middot;</span>
                                    <span><a href="#" class="text-xs">View All</a></span>
                                </div>

                                <div class="flex border-b border-solid border-grey-light">
                                    <div class="py-2 w-1/5">
                                        <a href="#">
                                            <img src="https://www.qualiscare.com/wp-content/uploads/2017/08/default-user.png" alt="follow1" class="rounded-full h-10 w-10" />
                                        </a>
                                    </div>
                                    <div class="pl-2 py-2 w-4/5">
                                        <div class="flex justify-between mb-1">
                                            <div>
                                                <a href="#" class="font-bold text-black">Nuxt.js</a> <a href="#" class="text-grey-dark">@nuxt_js</a>
                                            </div>

                                            <div>
                                                <a href="#" class="text-grey hover:text-grey-dark"><i class="fa fa-times"></i></a>
                                            </div>
                                        </div>
                                        <div>
                                            <button class="bg-transparent text-xs hover:bg-teal text-teal font-semibold hover:text-white py-2 px-6 border border-teal hover:border-transparent rounded-full">
                                            Follow
                                        </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="flex border-b border-solid border-grey-light">
                                    <div class="py-2 w-1/5">
                                        <a href="#">
                                            <img src="https://www.qualiscare.com/wp-content/uploads/2017/08/default-user.png" alt="follow1" class="rounded-full h-10 w-10" />
                                        </a>
                                    </div>
                                    <div class="pl-2 py-2 w-4/5">
                                        <div class="flex justify-between mb-1">
                                            <div>
                                                <a href="#" class="font-bold text-black">Laracon EU</a> <a href="#" class="text-grey-dark">@LaraconEU</a>
                                            </div>

                                            <div>
                                                <a href="#" class="text-grey hover:text-grey-dark"><i class="fa fa-times"></i></a>
                                            </div>
                                        </div>
                                        <div>
                                            <button class="bg-transparent text-xs hover:bg-teal text-teal font-semibold hover:text-white py-2 px-6 border border-teal hover:border-transparent rounded-full">
                                                Follow
                                            </button>
                                        </div>
                                    </div>
                                </div>
                </div>
                </div>
            </div>

        </div>
    );

  }
}

export default Twitter;